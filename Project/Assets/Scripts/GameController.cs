﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class GameController : MonoBehaviour
{
    public string towerCounterString = "Tower Count: {0}";
    public Text towerCounter;

    // Count of towers that causes entering the emergency mode.
    public int emergencyModeTowerCount = 100;

    public bool IsInEmergencyMode { get; private set; }

    private int towerCount;

    void Start()
    {
        towerCount = 1;
    }

    public void AddTower()
    {
        towerCount++;
        UpdateUI();

        if (towerCount >= emergencyModeTowerCount)
        {
            EnterEmergencyMode();
        }
    }

    public void RemoveTower()
    {
        towerCount--;
        UpdateUI();
    }

    public void EnterEmergencyMode()
    {
        foreach (GameObject towerObject in GameObject.FindGameObjectsWithTag("Tower"))
        {
            Tower tower = towerObject.GetComponent<Tower>();
            tower.ActivateTower();
            IsInEmergencyMode = true;
        }
    }

    private void UpdateUI()
    {
        towerCounter.text = string.Format(towerCounterString, towerCount);
    }
}