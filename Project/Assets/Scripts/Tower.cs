﻿using UnityEngine;
using System.Collections;

public class Tower : MonoBehaviour
{
    // How often does the tower rotate (in seconds)?
    public float rotationTimeStep = 0.5f;
    // Random rotation lower and upper boundaries.
    public float rotationLowerBound = 15.0f;
    public float rotationUpperBound = 45.0f;

    public GameObject ballPrefab;

    // Initial tower starts shooting right away. Other towers have to wait before they start rotating.
    public bool isInitialTower = false;
    public float activationDuration = 6.0f;

    public int shotsLimit = 12;

    // Colors.
    public Color activeColor = new Color(1.0f, 0.0f, 0.0f);
    public Color inactiveColor = new Color(1.0f, 1.0f, 1.0f);

    private int shotsCounter;
    private bool towerActive;
    private SpriteRenderer spriteRenderer;

    private GameController gameController;

    void Start()
    {
        shotsCounter = 0;
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();

        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        gameController.AddTower();

        if (isInitialTower)
        {
            ActivateTower();
        }
        else
        {
            StartCoroutine(ActivationCoroutine());
        }
    }

    public void ActivateTower()
    {
        bool wasActiveBefore = towerActive;
        towerActive = true;
        shotsCounter = 0;
        if (!wasActiveBefore)
        {
            spriteRenderer.color = activeColor;
            StartCoroutine(RotationCoroutine());
        }
    }

    private void DeactivateTower()
    {
        towerActive = false;
        spriteRenderer.color = inactiveColor;
    }

    private IEnumerator ActivationCoroutine()
    {
        yield return new WaitForSeconds(activationDuration);
        ActivateTower();
    }

    private IEnumerator RotationCoroutine()
    {
        while (towerActive)
        {
            Rotate();
            Shoot();
            yield return new WaitForSeconds(rotationTimeStep);
        }
    }

    private void Rotate()
    {
        float angle = Random.Range(rotationLowerBound, rotationUpperBound);
        transform.Rotate(Vector3.forward, angle);
    }

    private void Shoot()
    {
        // Spawn the ball with correct rotation.
        GameObject ballObject = (GameObject)Instantiate(ballPrefab, transform.position, transform.rotation);
        Ball ball = ballObject.GetComponent<Ball>();
        ball.SetParentTower(gameObject);

        shotsCounter++;
        if (shotsCounter >= shotsLimit)
        {
            DeactivateTower();
        }
    }
}