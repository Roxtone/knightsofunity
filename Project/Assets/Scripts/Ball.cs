﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    // Ball's speed in units/second.
    public float speed = 4.0f;
    // Random distance lower and upper boundaries.
    public float distanceLowerBound = 1.0f;
    public float distanceUpperBound = 4.0f;

    public GameObject towerPrefab;

    private float currentDistance;
    private float targetDistance;

    private GameObject parentTower;

    private GameController gameController;
    
    void Start()
    {
        currentDistance = 0.0f;
        targetDistance = Random.Range(distanceLowerBound, distanceUpperBound);

        gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }

    void Update()
    {
        if (currentDistance < targetDistance)
        {
            float distance = speed * Time.deltaTime;
            transform.Translate(transform.up * distance, Space.World);
            currentDistance += distance;
        }
        else
        {
            SpawnTower();
        }
    }

    private void SpawnTower()
    {
        if (!gameController.IsInEmergencyMode)
        {
            Instantiate(towerPrefab, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }

    // Collision handling.
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Tower" && other.gameObject != parentTower)
        {
            gameController.RemoveTower();
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    // Save tower that has shot the ball as we don't want to destroy it on collision.
    public void SetParentTower(GameObject tower)
    {
        parentTower = tower;
    }
}