﻿using UnityEngine;
using System.Collections;
public class NewtonBall : MonoBehaviour
{
    private GameObject dragger;
    private SpringJoint2D draggerJoint;

    void Start()
    {
        dragger = GameObject.FindGameObjectWithTag("Dragger");
        draggerJoint = dragger.GetComponent<SpringJoint2D>();
    }

    void OnMouseDrag()
    {
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newPosition.z = 0.0f;
        dragger.transform.position = newPosition;
        draggerJoint.connectedBody = rigidbody2D;
    }

    void OnMouseUp()
    {
        draggerJoint.connectedBody = null;
    }
}